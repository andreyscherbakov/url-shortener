<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UrlRepository")
 * @ORM\Table(name="url")
 */
class Url
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`url`", type="string", length=255, unique=true)
     * @Assert\NotBlank(message = "The url should not be blank")
     * @Assert\Url(message = "The url is not valid")
     * @CustomAssert\SelfServer
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="`key`", type="string", length=6, unique=true)
     */
    private $key;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }


    public function getId()
    {
        return $this->id;
    }
}
