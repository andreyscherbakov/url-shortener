<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Entity\Url;

class RedirectController extends Controller
{
    /**
     * @Route("/{key}", name="redirect", requirements={"key"="[A-Za-z0-9]{6}"})
     *
     */
    public function index($key)
    {
        $repository = $this->getDoctrine()->getRepository(Url::class);
        $urlEntity = $repository->findOneBy(
            ['key' => $key]
        );

        //if we found the url by key in the database - we redirect the customer to this url
        if ($urlEntity) {
            return $this->redirect($urlEntity->getUrl());
        }

        //otherwise we generate NotFoundHTTP Exception
        throw new NotFoundHttpException('Sorry, we coudn\'t find the short URL for this key!');
    }
}