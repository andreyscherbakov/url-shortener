<?php

//1. Make comments in the proper way

//3. See checks for SQLite server
//4. Tests
//5. Best practics
//6. Deploy

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UrlShortenType;
use App\Entity\Url;
use App\Utils;

class UrlShortenerController extends Controller
{
    /**
     * @var string
     */
    private $url = '';

    /**
     * @var string
     */
    private $shortUrl = '';

    /**
     * @var string
     */
    private $key = '';

    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(UrlShortenType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $urlEntity = $form->getData();
                $this->url = $urlEntity->getUrl();

                // if the url already exists in the database - we use the key for it
                //  otherwise we generate the new key and add new record
                if ( ! $this->isUrlAlreadyExists() ) {
                    $this->key = $this->generateKey();
                    $this->addNewUrl();
                }

                $this->shortUrl = $this->generateShortUrl();
            } else {
                $this->addFlash("error", "Unable to shorten the entered URL");
            }
        }

        return $this->render('index.html.twig', [
            'url_form' => $form->createView(),
            'url' => $this->url,
            'short_url' => $this->shortUrl
        ]);
    }

    // checks if the URL is already exists in the database and if it does - set the $key
    private function isUrlAlreadyExists(): bool
    {
        $repository = $this->getDoctrine()->getRepository(Url::class);
        $urlEntity = $repository->findOneBy(
            ['url' => $this->url]
        );
        if ($urlEntity) {
            $this->key = $urlEntity->getKey();
            return true;
        }
        return false;
    }

    private function addNewUrl(): void
    {
        $entityManager = $this->getDoctrine()->getManager();

        $urlEntity = new Url;
        $urlEntity->setUrl($this->url);
        $urlEntity->setKey($this->key);

        $entityManager->persist($urlEntity);
        $entityManager->flush();
    }

    private function generateKey(): string
    {
        return Utils\KeyGenerator::generate();
    }

    private function generateShortUrl(): string
    {
        return $this->generateUrl('home', [], UrlGeneratorInterface::ABSOLUTE_URL) . $this->key;
    }
}