<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class SelfServer extends Constraint
{
    public $message = 'That URL is already shorten';
}